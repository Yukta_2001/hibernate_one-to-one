package coms.coms.onetoone;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import coms.coms.onetoone.entity.Gender;
import coms.coms.onetoone.entity.User;
import coms.coms.onetoone.entity.UserProfile;
import coms.coms.onetoone.repository.UserProfileRepository;
import coms.coms.onetoone.repository.UserRepository;

@SpringBootApplication
public class Application implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserProfileRepository userProfileRepository;
	@Override
	public void run(String... args) throws Exception {
		//user Object
		
		User user = new User();
		user.setName("Ramesh");
		user.setEmail("ramesh@gmail.com");
		
		UserProfile userProfile = new UserProfile();
		userProfile.setAddress("Pune");
		userProfile.setBirthOfDate(LocalDate.of(2001, 01, 12));
		userProfile.setGender(Gender.MALE);
		userProfile.setPhoneNumber("9435448962");
		
		
		user.setUserProfile(userProfile);
		userProfile.setUser(user);
		
		
		userRepository.save(user);
	}

}
