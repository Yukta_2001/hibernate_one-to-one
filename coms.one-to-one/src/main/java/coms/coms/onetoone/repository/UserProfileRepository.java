package coms.coms.onetoone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import coms.coms.onetoone.entity.UserProfile;


@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long>{

}
